import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  UseGuards,
} from '@nestjs/common';

import { PersonService } from './person.service';
import { Person } from '../model/person.entity';
import { JwtAuthGuard } from '../auth/jwt-auth-guard';

@Controller('person')
export class PersonController {
  constructor(private personService: PersonService) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  async getPersons(): Promise<Person[]> {
    return this.personService.getPersons();
  }

  @Get('/:id')
  async getPersonById(@Param('id', ParseIntPipe) id: number): Promise<Person> {
    return this.personService.getPeopleById(id);
  }

  // @Post()
  // @UsePipes(ValidationPipe)
  // async createPerson(@Body() dto: CreatePersonDto): Promise<Person> {
  //   return this.personService.createPerson(dto);
  // }

  // @Post()
  // public async post(
  //   @User() user: User,
  //   @Body() dto: PersonDTO,
  // ): Promise<PersonDTO> {
  //   return this.personService.create(dto, user);
  // }
}
