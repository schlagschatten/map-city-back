import { Entity, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { RequestAboutProblem } from './request.entity';
import { Remark } from './remark.entity';

@Entity({ name: 'people' })
export class Person extends BaseEntity {
  @Column({ type: 'varchar', length: 400 })
  login: string;

  @Column({ type: 'varchar', length: 400 })
  password: string;

  @Column({ type: 'varchar', length: 400 })
  email: string;

  @OneToMany((type) => Remark, (remark) => remark.person)
  public remarks: Remark[];

  @OneToMany(
    (type) => RequestAboutProblem,
    (requestAboutProblem) => requestAboutProblem.person,
  )
  public requestAboutProblems: RequestAboutProblem[];
}
