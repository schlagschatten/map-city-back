import { Entity, Column, OneToMany, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { RequestAboutProblem } from './request.entity';
import { Remark } from './remark.entity';

@Entity({ name: 'categories' })
export class Category extends BaseEntity {
  @Column({ type: 'varchar', length: 300 })
  name: string;

  @Column({ type: 'varchar', length: 300 })
  color: string;

  @OneToMany(
    (type) => RequestAboutProblem,
    (requestAboutProblem) => requestAboutProblem.category,
  )
  public requestAboutProblems: RequestAboutProblem[];
}
