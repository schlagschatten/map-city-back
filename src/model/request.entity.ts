import { Entity, Column, OneToMany, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Person } from './person.entity';
import { Category } from './category.entity';
import { Remark } from './remark.entity';
import { Point } from 'geojson';

@Entity({ name: 'requestAboutProblems' })
export class RequestAboutProblem extends BaseEntity {
  @Column({ type: 'geometry', nullable: true })
  pointOnMap: Point;

  @Column({ type: 'boolean', nullable: true })
  repairsMade: boolean;

  @Column({ type: 'varchar', length: 300 })
  titleProblem: string;

  @Column({ type: 'varchar'})
  photo: string;

  @ManyToOne((type) => Person, (person) => person.requestAboutProblems)
  public person: Person;

  @ManyToOne((type) => Category, (category) => category.requestAboutProblems)
  public category: Category;

  @OneToMany((type) => Remark, (remark) => remark.requestAboutProblem)
  public remarks: Remark[];
}
