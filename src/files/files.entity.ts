import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../model/base.entity';
import { Remark } from '../model/remark.entity';

@Entity({ name: 'files' })
export class File extends BaseEntity {
  @Column()
  public name: string;

  @Column()
  public url: string;

  // @ManyToOne((type) => Remark, (remark) => remark.files, {
  //   onDelete: 'CASCADE',
  // })
  // public remark: Remark;
}
