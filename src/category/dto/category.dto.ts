import { IsString, IsUUID } from 'class-validator';
import { User } from '../../user/user.decorator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { Category } from '../../model/category.entity';

export class CategoryDTO implements Readonly<CategoryDTO> {
  @ApiModelProperty({ required: true })
  @IsUUID()
  id: number;

  @ApiModelProperty({ required: true })
  @IsString()
  name: string;

  @ApiModelProperty({ required: true })
  @IsString()
  color: string;

  public static from(dto: Partial<CategoryDTO>) {
    const it = new CategoryDTO();
    it.id = dto.id;
    it.name = dto.name;
    it.color = dto.color;
    return it;
  }

  public static fromEntity(entity: Category) {
    return this.from({
      id: entity.id,
      name: entity.name,
      color: entity.color,
    });
  }

  public toEntity(user: User = null) {
    const it = new Category();
    it.id = this.id;
    it.name = this.name;
    it.color = this.color;
    it.createDateTime = new Date();
    return it;
  }
}
