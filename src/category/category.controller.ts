import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { User } from '../user/user.decorator';
import { CategoryDTO } from './dto/category.dto';
import { CategoryService } from './category.service';
import { Category } from '../model/category.entity';
import { RequestAboutProblem } from '../model/request.entity';
import { CreateRemarkDto } from '../remark/dto/create-remark.dto';
import { Remark } from '../model/remark.entity';
import { CreateCategoryDto } from './dto/create-category.dto';

@Controller('category')
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get()
  public find(): Promise<Category[]> {
    return this.categoryService.find();
  }

  @Get('/:id')
  async getCategoryById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Category> {
    return this.categoryService.getCategoryById(id);
  }

  @Get('/:id/requestAboutProblems')
  public getRequestAboutProblems(
    @Param('id') id: number,
  ): Promise<RequestAboutProblem[]> {
    return this.categoryService.getRequestAboutProblems(id);
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createCategory(@Body() dto: CreateCategoryDto): Promise<Category> {
    return this.categoryService.createCategory(dto);
  }

  // @Post()
  // public async post(
  //   @User() user: User,
  //   @Body() dto: CategoryDTO,
  // ): Promise<CategoryDTO> {
  //   return this.categoryService.create(dto, user);
  // }
}
