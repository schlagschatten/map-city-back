import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryService } from './category.service';
import { Category } from '../model/category.entity';
import { CategoryController } from './category.controller';
import { RequestAboutProblem } from '../model/request.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Category, RequestAboutProblem])],
  providers: [CategoryService],
  controllers: [CategoryController],
  exports: [],
})
export class CategoryModule {}
