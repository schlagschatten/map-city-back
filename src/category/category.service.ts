import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/user.decorator';
import { Category } from '../model/category.entity';
import { CategoryDTO } from './dto/category.dto';
import { RequestAboutProblem } from '../model/request.entity';
import { CreateCategoryDto } from './dto/create-category.dto';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
    @InjectRepository(RequestAboutProblem)
    private readonly requestAboutProblemRepository: Repository<RequestAboutProblem>,
  ) {}

  // public async getAll(): Promise<CategoryDTO[]> {
  //   return await this.repo
  //     .find()
  //     .then((categories) => categories.map((e) => CategoryDTO.fromEntity(e)));
  // }

  public async find(): Promise<Category[]> {
    return this.categoryRepository.find();
  }

  async getCategoryById(id: number): Promise<Category> {
    const result = await this.categoryRepository.findOne(id);

    if (!result) {
      throw new NotFoundException(`Категорія з ID: ${id} не існує`);
    }
    return result;
  }

  public async getRequestAboutProblems(
    id: number,
  ): Promise<RequestAboutProblem[]> {
    return this.requestAboutProblemRepository
      .createQueryBuilder('requestAboutProblem')
      .where('requestAboutProblem.category = :categoryId', { categoryId: id })
      .getMany();
  }

  public async createCategory(dto: CreateCategoryDto): Promise<Category> {
    const { ...otherFields } = dto;
    return this.categoryRepository.save({
      ...otherFields,
    });
  }
}
