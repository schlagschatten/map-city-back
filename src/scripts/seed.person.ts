// import * as _ from 'lodash';
// import { createConnection, ConnectionOptions } from 'typeorm';
// import { configService } from '../config/config.service';
// import { User } from '../user/user.decorator';
// import * as people from './data/people.json';
// import { PersonDTO } from '../person/dto/person.dto';
// import { PersonService } from '../person/person.service';
// import { Person } from '../model/person.entity';
//
// async function run() {
//   const seedUser: User = { id: 'seed-user' };
//
//   const seedId = Date.now()
//     .toString()
//     .split('')
//     .reverse()
//     .reduce((s, it, x) => (x > 3 ? s : (s += it)), '');
//
//   const opt = {
//     ...configService.getTypeOrmConfig(),
//     debug: true,
//   };
//
//   const connection = await createConnection(opt as ConnectionOptions);
//   const itemService = new PersonService(connection.getRepository(Person));
//
//   const work = people
//     .map((person) => PersonDTO.from(person))
//     .map((dto) =>
//       itemService
//         .create(dto, seedUser)
//         .then((r) => (console.log('done ->', r.login, r.email, r.password), r)),
//     );
//
//   return await Promise.all(work);
// }
//
// run()
//   .then((_) => console.log('...wait for script to exit'))
//   .catch((error) => console.error('seed error', error));
