// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { configService } from '../config/config.service';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import fs = require('fs');

fs.writeFileSync(
  'ormconfig.json',
  JSON.stringify(configService.getTypeOrmConfig(), null, 2),
);
