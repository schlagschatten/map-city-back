// import * as _ from 'lodash';
// import { createConnection, ConnectionOptions } from 'typeorm';
// import { configService } from '../config/config.service';
// import { User } from '../user/user.decorator';
// import { RequestAboutProblemDto } from '../requestAboutProblem/dto/requestAboutProblem.dto';
// import { RequestAboutProblem } from '../model/requestAboutProblem.entity';
// import { RequestAboutProblemService } from '../requestAboutProblem/requestAboutProblem.service';
// import * as requestAboutProblems from './data/requestAboutProblems.json';
// import { Category } from '../model/category.entity';
// import {Person} from "../model/person.entity";
//
// async function run() {
//   const seedUser: User = { id: 'seed-user' };
//
//   const seedId = Date.now()
//     .toString()
//     .split('')
//     .reverse()
//     .reduce((s, it, x) => (x > 3 ? s : (s += it)), '');
//
//   const opt = {
//     ...configService.getTypeOrmConfig(),
//     debug: true,
//   };
//
//   const connection = await createConnection(opt as ConnectionOptions);
//
//   const requestAboutProblemService = new RequestAboutProblemService(
//     connection.getRepository(RequestAboutProblem),
//     connection.getRepository(Category),
//       connection.getRepository(Person)
//   );
//
//   const work = requestAboutProblems
//     .map((requestAboutProblem) =>
//       RequestAboutProblemDto.from(requestAboutProblem),
//     )
//     .map((dto) =>
//       requestAboutProblemService
//         .create(dto, seedUser)
//         .then((r) => (console.log('done ->', r.pointOnMap, r.repairsMade), r)),
//     );
//
//   return await Promise.all(work);
// }
//
// run()
//   .then((_) => console.log('...wait for script to exit'))
//   .catch((error) => console.error('seed error', error));
