import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';
import { RequestAboutProblemModule } from './requestAboutProblem/requestAboutProblem.module';
import { PersonModule } from './person/person.module';
import { CategoryModule } from './category/category.module';
import { RemarkModule } from './remark/remark.module';
import { AuthModule } from './auth/auth.module';
import { JwtStrategy } from './auth/jwt.strategy';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    RequestAboutProblemModule,
    PersonModule,
    CategoryModule,
    RemarkModule,
    AuthModule
  ]
})
export class AppModule {}
