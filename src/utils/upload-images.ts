import { BadRequestException } from '@nestjs/common';
import { Request } from 'express';
import { extname } from 'path';
import { IFile } from 'src/remark/remark';
import * as uuid from 'uuid';

export const imageFileFilter = (
  req: Request,
  file: IFile,
  callback: CallableFunction,
): void => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    callback(
      new BadRequestException(
        'Завантажити можливо лише файли зображень .jpg, .jpeg, .png, .gif',
      ),
      false,
    );
  }

  callback(null, true);
};

export const customImageFileName = (
  req: Request,
  file: IFile,
  callback: CallableFunction,
): void => {
  const fileExt = extname(file.originalname);
  const fileName = file.originalname.split('.')[0];

  callback(null, `${fileName}-${uuid.v1()}${fileExt}`);
};
