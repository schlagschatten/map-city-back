import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/user.decorator';
import { Remark } from '../model/remark.entity';
import { RemarkDTO } from './dto/remark.dto';
import { RequestAboutProblem } from '../model/request.entity';
import { Person } from '../model/person.entity';
import { CreateRemarkDto } from './dto/create-remark.dto';

@Injectable()
export class RemarkService {
  constructor(
    @InjectRepository(Remark)
    private readonly remarkRepository: Repository<Remark>,
    @InjectRepository(Person)
    private readonly personRepository: Repository<Person>,
    @InjectRepository(RequestAboutProblem)
    private readonly requestAboutProblemRepository: Repository<RequestAboutProblem>,
  ) {}
  async getRemark(): Promise<Remark[]> {
    return this.remarkRepository.find({
      relations: ['person', 'requestAboutProblem'],
    });
  }

  async getRemarksById(id: number): Promise<Remark> {
    const result = await this.remarkRepository.findOne(id, {
      relations: ['person', 'requestAboutProblem'],
    });

    if (!result) {
      throw new NotFoundException(`Remarks with ID: ${id} not found`);
    }

    return result;
  }

  async getRemarksByPersonId(personId: number): Promise<Remark[]> {
    return await this.remarkRepository.find({
      where: { person: personId },
      relations: ['person'],
    });
  }

  async getRemarksByRequestAboutProblemId(
    requestAboutProblemId: number,
  ): Promise<Remark[]> {
    return await this.remarkRepository.find({
      where: { requestAboutProblem: requestAboutProblemId },
      relations: ['requestAboutProblem'],
    });
  }

  async deleteRemark(id: number) {
    const remark = await this.remarkRepository.findOne(id);

    if (!remark) {
      throw new NotFoundException(`Remark with ID: ${id} not found`);
    }

    await this.remarkRepository.delete(id);

    return { message: `Remark with ID: ${id} has been deleted` };
  }

  async createRemark(dto: CreateRemarkDto): Promise<Remark> {
    const { personId, requestAboutProblemId, ...otherFields } = dto;
    const relatedPerson = await this.personRepository.findOne({
      where: { id: personId },
    });

    if (!relatedPerson) {
      throw new NotFoundException(`Категорія з назвою ${personId} не існує`);
    }
    const relatedRequestAboutProblem = await this.requestAboutProblemRepository.findOne(
      {
        where: { id: requestAboutProblemId },
      },
    );

    if (!relatedRequestAboutProblem) {
      throw new NotFoundException(
        `Людина з id ${requestAboutProblemId} не існує`,
      );
    }

    return this.remarkRepository.save({
      ...otherFields,
      person: relatedPerson,
      requestAboutProblem: relatedRequestAboutProblem,
    });
  }

  public async create(dto: RemarkDTO, user: User): Promise<RemarkDTO> {
    return this.remarkRepository
      .save(dto.toEntity(user))
      .then((e) => RemarkDTO.fromEntity(e));
  }
}
