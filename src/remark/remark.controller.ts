import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { User } from '../user/user.decorator';
import { RemarkService } from './remark.service';
import { RemarkDTO } from './dto/remark.dto';
import { Remark } from '../model/remark.entity';
import { CreateRemarkDto } from './dto/create-remark.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth-guard';

@Controller('remark')
export class RemarkController {
  constructor(private remarkService: RemarkService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async getRemarks(): Promise<Remark[]> {
    return this.remarkService.getRemark();
  }

  @Get('/:id')
  async getProductById(@Param('id', ParseIntPipe) id: number): Promise<Remark> {
    return this.remarkService.getRemarksById(id);
  }

  @Get('/person/:personId')
  async getRemarksByPersonId(
    @Param('personId', ParseIntPipe) personId: number,
  ): Promise<Remark[]> {
    return this.remarkService.getRemarksByPersonId(personId);
  }

  @Get('/category/:requestAboutProblemId')
  async getProductsByCategoryId(
    @Param('requestAboutProblemId', ParseIntPipe) requestAboutProblemId: number,
  ): Promise<Remark[]> {
    return this.remarkService.getRemarksByRequestAboutProblemId(
      requestAboutProblemId,
    );
  }

  @Delete('/:id')
  async deleteRemark(@Param('id', ParseIntPipe) id: number) {
    return this.remarkService.deleteRemark(id);
  }

  //POST

  @Post()
  @UsePipes(ValidationPipe)
  async createRemark(@Body() dto: CreateRemarkDto): Promise<Remark> {
    return this.remarkService.createRemark(dto);
  }

  @Post()
  public async post(
    @User() user: User,
    @Body() dto: RemarkDTO,
  ): Promise<RemarkDTO> {
    return this.remarkService.create(dto, user);
  }
}
