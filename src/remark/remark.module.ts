import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RemarkService } from './remark.service';
import { RemarkController } from './remark.controller';
import { Remark } from '../model/remark.entity';
import { Person } from '../model/person.entity';
import { RequestAboutProblem } from '../model/request.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Remark, Person, RequestAboutProblem])],
  providers: [RemarkService],
  controllers: [RemarkController],
  exports: [],
})
export class RemarkModule {}
