import { IsNotEmpty, IsNumber, IsObject, IsString } from 'class-validator';

export class CreateRemarkDto {
  @IsNotEmpty()
  @IsString()
  text: string;

  @IsNotEmpty()
  @IsNumber()
  personId: number;

  @IsNotEmpty()
  @IsNumber()
  requestAboutProblemId: number;
}
