import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PersonService } from '../person/person.service';
import { JwtService } from '@nestjs/jwt';
import { CreatePersonDto } from '../person/dto/create-person.dto';
import { LoginPersonDto } from '../person/dto/login-person.dto';
import { JwtPayload } from './JwtPayload';
import { PersonDTO } from '../person/dto/person.dto';

export interface RegistrationStatus {
  success: boolean;
  message: string;
}

@Injectable()
export class AuthService {
  constructor(
    private readonly personService: PersonService,
    private readonly jwtService: JwtService,
  ) {}

  async register(personDto: CreatePersonDto): Promise<RegistrationStatus> {
    let status: RegistrationStatus = {
      success: true,
      message: 'user registered',
    };
    try {
      await this.personService.createPerson(personDto);
    } catch (err) {
      status = {
        success: false,
        message: err,
      };
    }
    return status;
  }
  async login(loginPersonDto: LoginPersonDto): Promise<LoginStatus> {
    // find user in db
    const user = await this.personService.findByLogin(loginPersonDto);

    // generate and sign token
    const token = this._createToken(user);

    return {
      personId: user.id,
      login: user.login,
      ...token,
    };
  }

  private _createToken({ login }: PersonDTO): Token {
    const user: JwtPayload = { login };
    const accessToken = this.jwtService.sign(user);
    return {
      // time in minutes
      expiresIn: process.env.EXPIRESIN + 'm',
      accessToken,
    };
  }

  async validateUser(payload: JwtPayload): Promise<PersonDTO> {
    const person = await this.personService.findByPayload(payload);
    if (!person) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    return person;
  }
}

interface Token {
  accessToken: any;
  expiresIn: any;
}

export interface LoginStatus extends Token {
  personId: number,
  login: string;
}
