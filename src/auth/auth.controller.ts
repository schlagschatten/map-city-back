import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
} from '@nestjs/common';
import { AuthService, LoginStatus, RegistrationStatus } from './auth.service';
import { CreatePersonDto } from '../person/dto/create-person.dto';
import { LoginPersonDto } from '../person/dto/login-person.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  public async register(
    @Body() createPersonDto: CreatePersonDto,
  ): Promise<RegistrationStatus> {
    const result: RegistrationStatus = await this.authService.register(
      createPersonDto,
    );
    if (!result.success) {
      throw new HttpException(result.message, HttpStatus.BAD_REQUEST);
    }
    return result;
  }

  @Post('login')
  public async login(
    @Body() loginPersonDto: LoginPersonDto,
  ): Promise<LoginStatus> {
    return await this.authService.login(loginPersonDto);
  }
}
