import {
  IsBoolean,
  IsNumber,
  IsObject,
  IsString,
  IsUUID,
} from 'class-validator';
import { User } from '../../user/user.decorator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { RequestAboutProblem } from '../../model/request.entity';
import { CategoryDTO } from '../../category/dto/category.dto';

export class RequestAboutProblemDto
  implements Readonly<RequestAboutProblemDto> {
  @ApiModelProperty({ required: true })
  @IsNumber()
  //@IsUUID()
  id: number;

  @ApiModelProperty({ required: true })
  @IsObject()
  pointOnMap: Object;

  @ApiModelProperty({ required: true })
  @IsBoolean()
  repairsMade: boolean;

  @ApiModelProperty({ required: true })
  @IsString()
  titleProblem: string;

  @ApiModelProperty({ required: true })
  @IsString()
  photo: string;

  @ApiModelProperty({ required: true })
  @IsUUID()
  personId: string;

  @ApiModelProperty({ required: true })
  @IsUUID()
  categoryId: number;

  @ApiModelProperty({ required: true })
  category: Partial<CategoryDTO>;

  public static from(dto: Partial<RequestAboutProblemDto>) {
    const it = new RequestAboutProblemDto();
    it.id = dto.id;
    it.pointOnMap = dto.pointOnMap;
    it.repairsMade = dto.repairsMade;
    it.titleProblem = dto.titleProblem;
    it.photo = dto.photo;
    it.category = CategoryDTO.from(dto.category);
    return it;
  }

  public static fromEntity(entity: RequestAboutProblem) {
    return this.from({
      id: entity.id,
      pointOnMap: entity.pointOnMap,
      repairsMade: entity.repairsMade,
      titleProblem: entity.titleProblem,
      photo: entity.photo,
    });
  }

  public toEntity(user: User = null) {
    const it = new RequestAboutProblem();
    it.id = this.id;
    it.pointOnMap = this.pointOnMap;
    it.repairsMade = this.repairsMade;
    it.titleProblem = this.titleProblem;
    it.photo = this.photo;
    it.createDateTime = new Date();
    return it;
  }
}
