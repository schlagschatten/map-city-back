import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsString,
} from 'class-validator';

export class CreateRequestAboutProblemDto {
  @IsNotEmpty()
  @IsObject()
  pointOnMap: Object;

  @IsNotEmpty()
  @IsBoolean()
  repairsMade: boolean;

  @IsNotEmpty()
  @IsString()
  titleProblem: string;

  @IsNotEmpty()
  @IsString()
  photo: string;

  @IsNotEmpty()
  @IsNumber()
  categoryId: number;

  @IsNotEmpty()
  @IsNumber()
  personId: number;
}
