import { IsNotEmpty, IsBoolean } from 'class-validator';

export class UpdateProblemVisibility {
  @IsNotEmpty()
  @IsBoolean()
  repairsMade: boolean;
}
