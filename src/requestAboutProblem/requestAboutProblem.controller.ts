import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post, Put,
  Res,
  UploadedFile,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { User } from '../user/user.decorator';
import { RequestAboutProblem } from '../model/request.entity';
import { CreateRequestAboutProblemDto } from './dto/create-requestAboutProblem.dto';
import { RequestAboutProblemService } from './requestAboutProblem.service';
import { RequestAboutProblemDto } from './dto/requestAboutProblem.dto';
import { Response } from 'express';
import { FileInterceptor } from '@nestjs/platform-express';
import { customImageFileName, imageFileFilter } from '../utils/upload-images';
import { IFile } from '../remark/remark';
import { File } from '../files/files.entity';
import { diskStorage } from 'multer';
import { UpdateProblemVisibility } from './dto/update-problem-visibility';

@Controller('requestAboutProblem')
export class RequestAboutProblemController {
  constructor(private requestAboutProblemService: RequestAboutProblemService) {}

  // @Get()
  // public async getAll(): Promise<RequestAboutProblemDTO[]> {
  //   return await this.serv.getAll();
  // }

  // GET
  @Get()
  async getRequestAboutProblems(): Promise<RequestAboutProblem[]> {
    return this.requestAboutProblemService.getRequestAboutProblem();
  }

  @Get('/:id')
  async getRequestAboutProblemById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<RequestAboutProblem> {
    return this.requestAboutProblemService.getRequestAboutProblemsById(id);
  }

  @Get('/img/:imgName')
  async getRequestAboutProblemImage(
    @Param('imgName') imgName: string,
    @Res() res: Response,
  ): Promise<void> {
    return this.requestAboutProblemService.getRequestAboutProblemImage(
      imgName,
      res,
    );
  }

  @Get('/category/:categoryId')
  async getProductsByCategoryId(
    @Param('categoryId', ParseIntPipe) categoryId: number,
  ): Promise<RequestAboutProblem[]> {
    return this.requestAboutProblemService.getRequestAboutProblemsByCategoryId(
      categoryId,
    );
  }

  //DELETE
  @Delete('/:id')
  async deleteRequestAboutProblems(@Param('id', ParseIntPipe) id: number) {
    return this.requestAboutProblemService.deleteRequestAboutProblems(id);
  }

  @Delete('/img/:imgName')
  async deleteImageFile(@Param('imgName') imgName: string) {
    return this.requestAboutProblemService.deleteRequestAboutProblemImageFile(
      imgName,
    );
  }

  //POST

  @Post('images')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: 'src/uploads/images',
        filename: customImageFileName,
      }),
      fileFilter: imageFileFilter,
    }),
  )
  @UsePipes(ValidationPipe)
  async uploadRequestAboutProblemPhoto(
    @UploadedFile() file: IFile,
  ): Promise<File> {
    return this.requestAboutProblemService.uploadRequestAboutProblemImage(file);
  }

  @Post()
  @UsePipes(ValidationPipe)
  async createRequestAboutProblem(
    @Body() dto: CreateRequestAboutProblemDto,
  ): Promise<RequestAboutProblem> {
    return this.requestAboutProblemService.createRequestAboutProblem(dto);
  }

  @Post()
  public async post(
    @User() user: User,
    @Body() dto: RequestAboutProblemDto,
  ): Promise<RequestAboutProblemDto> {
    return this.requestAboutProblemService.create(dto, user);
  }

  @Put('/visibility/:id')
  async updateProblemVisibility(
    @Param('id', ParseIntPipe) id: number,
    @Body() dto: UpdateProblemVisibility,
  ): Promise<RequestAboutProblem> {
    return this.requestAboutProblemService.updateProblemVisibility(id, dto);
  }
}
